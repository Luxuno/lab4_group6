import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'main.dart';

class logoutPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color.fromRGBO(49, 53, 56, 1),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.only(bottom: 50, top: 150),
            child:Image.asset('images/imgurLogo.png'),
          ),
          Center(
            child: RaisedButton(
              child: Text("Log In"),
              color: Colors.green,
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => imgurHome(),
                  ),
                );
              },
            ),
          )
        ],
      ),
    );
  }
}