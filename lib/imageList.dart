import 'dart:math';
var rng = new Random();

List<String> row1items = [
  'images/coolio.PNG',
  'images/Y65t9QS.png',
  'images/Eh.PNG',
  'images/PYROMANCY.png',
  'images/TerrariaAircraftCarrier.png',
  'images/gameDevProgress.jpg',
  'images/programmerMeme.jpg',
  'images/cursedImage.jpg',
  'images/stonksIsReal.jpg',
];

List<String> row1titles = [
  'Finally reached level 80~!',
  'Windwaker Lonk',
  'Oriental-styled Smudge the cat',
  'Elmos NEW world',
  'My aircraft carrier build in Terraria',
  'New character for my game dev project',
  'Programmers these days',
  'Cursed keyboard',
  'I thought he looked familiar',
];

List<List> row1values = [
  [rng.nextInt(100)+5,rng.nextInt(100),rng.nextInt(100)],
  [rng.nextInt(100)+5,rng.nextInt(100),rng.nextInt(100)],
  [rng.nextInt(100)+5,rng.nextInt(100),rng.nextInt(100)],
  [rng.nextInt(100)+5,rng.nextInt(100),rng.nextInt(100)],
  [rng.nextInt(100)+5,rng.nextInt(100),rng.nextInt(100)],
  [rng.nextInt(100)+5,rng.nextInt(100),rng.nextInt(100)],
  [rng.nextInt(100)+5,rng.nextInt(100),rng.nextInt(100)],
  [rng.nextInt(100)+5,rng.nextInt(100),rng.nextInt(100)],
  [rng.nextInt(100)+5,rng.nextInt(100),rng.nextInt(100)],
];

List<String> row2items = [
  'images/Amazing..jpg',
  'images/connectFork.jpg',
  'images/derpyCat.jpg',
  'images/EEEEEHHHHHH.jpg',
  'images/expectNothing.jpg',
  'images/faces.jpg',
  'images/glockOnACroc.jpg',
  'images/itsMyStyle.jpg',
  'images/moneyDog.jpg',
];

List<String> row2titles = [
  'If Rob Liefeld directed Captain America',
  'Connect Fork',
  'My derpy cat',
  'Dude, chill',
  'Totally not a repost',
  'A face in my backpack',
  'Forget Elf on a Shelf, get ready for...',
  'When they say "its just my style" ',
  'This is Money Dog',
];

List<List> row2values = [
  [rng.nextInt(100)+5,rng.nextInt(100),rng.nextInt(100)],
  [rng.nextInt(100)+5,rng.nextInt(100),rng.nextInt(100)],
  [rng.nextInt(100)+5,rng.nextInt(100),rng.nextInt(100)],
  [rng.nextInt(100)+5,rng.nextInt(100),rng.nextInt(100)],
  [rng.nextInt(100)+5,rng.nextInt(100),rng.nextInt(100)],
  [rng.nextInt(100)+5,rng.nextInt(100),rng.nextInt(100)],
  [rng.nextInt(100)+5,rng.nextInt(100),rng.nextInt(100)],
  [rng.nextInt(100)+5,rng.nextInt(100),rng.nextInt(100)],
  [rng.nextInt(100)+5,rng.nextInt(100),rng.nextInt(100)],
];

List<String> row3items = [
  'images/nowthisispodracing.png',
  'images/PepeSilvia.png',
  'images/soaked.png',
  'images/SpongeMarines.jpg',
  'images/superNintendoChalmers.jpg',
  'images/donkeyBonk.png',
  'images/greenLanternGuy.jpg',
  'images/noob.jpg',
  'images/wholesomeAliens.jpg',
];

List<String> row3titles = [
  'Totally not another repost',
  'When it all starts to come together',
  'Dunk me',
  'Sponge Marines',
  'Super Nintendo Chalmers',
  'Donkey Bonk',
  'This funny Green Lantern Guy',
  'Why is that even there?!',
  'Wholesome Aliens playing video games',
];

List<List> row3values = [
  [rng.nextInt(100)+5,rng.nextInt(100),rng.nextInt(100)],
  [rng.nextInt(100)+5,rng.nextInt(100),rng.nextInt(100)],
  [rng.nextInt(100)+5,rng.nextInt(100),rng.nextInt(100)],
  [rng.nextInt(100)+5,rng.nextInt(100),rng.nextInt(100)],
  [rng.nextInt(100)+5,rng.nextInt(100),rng.nextInt(100)],
  [rng.nextInt(100)+5,rng.nextInt(100),rng.nextInt(100)],
  [rng.nextInt(100)+5,rng.nextInt(100),rng.nextInt(100)],
  [rng.nextInt(100)+5,rng.nextInt(100),rng.nextInt(100)],
  [rng.nextInt(100)+5,rng.nextInt(100),rng.nextInt(100)],
];