import 'package:flutter/material.dart';
import 'main.dart';
import 'LogoutPage.dart';
import 'FavoritesPage.dart';

class settingsPage extends StatefulWidget {
  @override
  settingsPageState createState() => settingsPageState();
}

class settingsPageState extends State<settingsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: NavDrawer(),
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Image.asset('images/imgurLogo.png', fit: BoxFit.fill,width: 75,),
            PopupMenuButton(
                itemBuilder: (context) => [
                  PopupMenuItem(
                    value: 1,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Icon(
                          Icons.verified_user,
                          color: Colors.black,
                        ),
                        Text('Profile'),
                      ],
                    ),
                  ),
                  PopupMenuItem(
                    value: 2,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Icon(
                          Icons.settings,
                          color: Colors.black,
                        ),
                        FlatButton(
                          child: Text('Settings'),
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => (logoutPage()),
                              ),
                            );
                            setState(() {
                            });
                          },
                        ),
                      ],
                    ),
                  ),
                  PopupMenuItem(
                    value: 3,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Icon(
                          Icons.exit_to_app,
                          color: Colors.black,
                        ),
                        FlatButton(
                          child: Text('Logout'),
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => (logoutPage()),
                              ),
                            );
                            setState(() {
                            });
                          },
                        ),
                      ],
                    ),
                  ),
                ])
          ],
        ),
        backgroundColor: Colors.blueGrey,
      ),
      body: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              _buildSettingsButton('Account', 16, FontWeight.w400),
              _buildSettingsButton('Brightness', 16, FontWeight.w400),
              _buildSettingsButton('Language', 16, FontWeight.w400),
              _buildSettingsButton('About', 16, FontWeight.w400),
              _buildSettingsButton('Feedback', 16, FontWeight.w400),
              _buildSettingsButton('Help', 16, FontWeight.w400),

            ],
          )

        ),
    );
  }
  FlatButton _buildSettingsButton(String label, double fontSize, FontWeight weight)
  {
    return FlatButton(
        height: 70,
        child: Align(
          child:Text(label,
            style: TextStyle(fontSize: fontSize, fontWeight: weight),
          ),
          alignment: Alignment.centerLeft,
        ),
        hoverColor: Colors.grey,
        onPressed: () {
          setState(() { });
        }
    );
  }
}


//Used code from https://medium.com/@maffan/how-to-create-a-side-menu-in-flutter-a2df7833fdfb
//to build the side menu of our app
class NavDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        color: Colors.green,
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              decoration: BoxDecoration(
                  color: Colors.green,
                  image: DecorationImage(
                    fit: BoxFit.fill,
                    image: AssetImage('images/imgur.png'),
                  )),
            ),
            ListTile(
              leading: Icon(Icons.verified_user),
              title: Text('Profile'),
              onTap: () => {Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => imgurHome(),
              ),
              )},
            ),
            ListTile(
              leading: Icon(Icons.settings),
              title: Text('Settings'),
              onTap: () => {Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => settingsPage(),
              ),
              )},
            ),
            ListTile(
              leading: Icon(Icons.favorite),
              title: Text('Favorite Images'),
              onTap: () => {Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => (favoritesPage()),
                ),
              )},
            ),
            ListTile(
                leading: Icon(Icons.exit_to_app),
                title: Text('Logout'),
                onTap: () => {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => (logoutPage()),
                    ),
                  )
                }
            ),
          ],
        ),
      ),
    );
  }
}
