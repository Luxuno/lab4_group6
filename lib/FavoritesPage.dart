import 'package:flutter/material.dart';
import 'package:lab4_group6/SettingsPage.dart';
import 'imageList.dart';
import 'main.dart';
import 'logoutPage.dart';
import 'main.dart';
import 'main.dart';


class favoritesPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: NavDrawer(),
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Image.asset('images/imgurLogo.png', fit: BoxFit.fill,width: 75,),
            PopupMenuButton(
                itemBuilder: (context) => [
                  PopupMenuItem(
                    value: 1,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Icon(
                          Icons.verified_user,
                          color: Colors.black,
                        ),
                        FlatButton(
                          child: Text('Profile'),
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => (imgurHome()),
                              ),
                            );
                          },
                        ),
                      ],
                    ),
                  ),
                  PopupMenuItem(
                    value: 2,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Icon(
                          Icons.settings,
                          color: Colors.black,
                        ),
                        FlatButton(
                          child: Text('Settings'),
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => (settingsPage()),
                              ),
                            );
                          },
                        ),
                      ],
                    ),
                  ),
                  PopupMenuItem(
                    value: 3,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Icon(
                          Icons.exit_to_app,
                          color: Colors.black,
                        ),
                        FlatButton(
                          child: Text('Logout'),
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => (logoutPage()),
                              ),
                            );
                          },
                        ),
                      ],
                    ),
                  ),
                ])
          ],
        ),
        backgroundColor: Colors.blueGrey,
      ),
      body: Center(
          child: Container(
            color: Color.fromRGBO(49, 53, 56, 1),
            child: GridView.count(
              crossAxisCount: 4,
              children: _buildFavoritesList(3, 3, Colors.grey[900]),
            ),
          ),
      ),
    );
  }
}

List<Container> _buildFavoritesList(double margin, double borderWidth, Color color) {
  List<Container> favorites = new List<Container>();

  for(int i = 0; i < row1items.length; i++)
  {
    favorites.add(Container(
      margin: EdgeInsets.all(margin),
      decoration: BoxDecoration(
        border: Border.all(
          color: color,
          width: borderWidth,
        ),
      ),
      child: Image.asset(row1items[i], fit: BoxFit.fill,),
    ));
  }

  for(int i = 0; i < row2items.length; i++)
  {
    favorites.add(Container(
      margin: EdgeInsets.all(margin),
      decoration: BoxDecoration(
        border: Border.all(
          color: color,
          width: borderWidth,
        ),
      ),
      child: Image.asset(row2items[i], fit: BoxFit.fill,),
    ));
  }

  for(int i = 0; i < row3items.length; i++)
  {
    favorites.add(Container(
      margin: EdgeInsets.all(margin),
      decoration: BoxDecoration(
        border: Border.all(
          color: color,
          width: borderWidth,
        ),
      ),
      child: Image.asset(row3items[i], fit: BoxFit.fill,),
    ));
  }

  return favorites;
}

//Used code from https://medium.com/@maffan/how-to-create-a-side-menu-in-flutter-a2df7833fdfb
//to build the side menu of our app
class NavDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        color: Colors.green,
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              decoration: BoxDecoration(
                  color: Colors.green,
                  image: DecorationImage(
                    fit: BoxFit.fill,
                    image: AssetImage('images/imgur.png'),
                  )),
            ),
            ListTile(
              leading: Icon(Icons.verified_user),
              title: Text('Profile'),
              onTap: () => {Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => (imgurHome()),
                ),
              )},
            ),
            ListTile(
              leading: Icon(Icons.settings),
              title: Text('Settings'),
              onTap: () => {Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => (settingsPage()),
                ),
              )},
            ),
            ListTile(
              leading: Icon(Icons.favorite),
              title: Text('Favorite Images'),
              onTap: () => {Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => (favoritesPage()),
                ),
              )},
            ),
            ListTile(
                leading: Icon(Icons.exit_to_app),
                title: Text('Logout'),
                onTap: () => {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => (logoutPage()),
                    ),
                  )
                }
            ),
          ],
        ),
      ),
    );
  }
}
