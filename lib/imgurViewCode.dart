import 'package:flutter/material.dart';

class imgurTop extends StatefulWidget {
  List<String> imageItems;
  List<String> titleItems;
  List<List> items;

  imgurTop(this.imageItems, this.titleItems, this.items);

  @override
  _imgurTopState createState() => _imgurTopState();
}

//Contains the Daily Top Pictures Row
class _imgurTopState extends State<imgurTop> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 350,
      child: ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        itemCount: widget.imageItems.length,
        itemBuilder: (BuildContext context, int index) => Column(
          children: [
            Container(
              padding: EdgeInsets.all(5),
              width: 225,
              //Color of the card and its appearance
              child: Card(
                color: Color.fromRGBO(82, 82, 82, 1),
                elevation: 10,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0)),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    //Image asset
                    ClipRRect(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(10),
                          topRight: Radius.circular(10)),
                      child: ConstrainedBox(
                        constraints: BoxConstraints(
                          maxHeight: 200,
                        ),
                        child: Image.asset(
                          widget.imageItems[index],
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                    SizedBox(height: 10),
                    //OP's comment
                    Text(
                      widget.titleItems[index],
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(height: 10),
                    //Contains
                    Row(
                      //mainAxisAlignment: MainAxisAlignment.spaceAround,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        InputChip(
                          label: Text(
                            widget.items[index][0].toString(),
                            style: TextStyle(color: Colors.black),
                          ),
                          avatar: Icon(
                            Icons.arrow_upward,
                            color: Colors.black,
                          ),
                        ),
                        InputChip(
                          label: Text(
                            widget.items[index][1].toString(),
                            style: TextStyle(color: Colors.black),
                          ),
                          avatar: Icon(
                            Icons.textsms,
                            color: Colors.black,
                          ),
                        ),
                        InputChip(
                          label: Text(
                            widget.items[index][2].toString(),
                            style: TextStyle(color: Colors.black),
                          ),
                          avatar: Icon(
                            Icons.remove_red_eye,
                            color: Colors.black,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

/*
  //Contains the card for images
  Column CardClass(String imageAsset, String imageComment, List<int> values) {
    //Used Column to have card be sized to content
    return Column(
      children: [
        Container(
          padding: EdgeInsets.all(5),
          width: 225,
          //Color of the card and its appearance
          child: Card(
            color: Color.fromRGBO(43, 43, 43, 1),
            elevation: 10,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0)),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                //Image asset
                ClipRRect(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      topRight: Radius.circular(10)),
                  child: Image.asset(
                    imageAsset,
                    fit: BoxFit.fill,
                  ),
                ),
                SizedBox(height: 10),
                //OP's comment
                Text(
                  imageComment,
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold),
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 10),
                //Contains
                Row(
                  //mainAxisAlignment: MainAxisAlignment.spaceAround,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    InputChip(
                      label: Text(
                        values[0].toString(),
                        style: TextStyle(color: Colors.black),
                      ),
                      avatar: Icon(
                        Icons.arrow_upward,
                        color: Colors.black,
                      ),
                    ),
                    InputChip(
                      label: Text(
                        values[1].toString(),
                        style: TextStyle(color: Colors.black),
                      ),
                      avatar: Icon(
                        Icons.textsms,
                        color: Colors.black,
                      ),
                    ),
                    InputChip(
                      label: Text(
                        values[2].toString(),
                        style: TextStyle(color: Colors.black),
                      ),
                      avatar: Icon(
                        Icons.remove_red_eye,
                        color: Colors.black,
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

   */
}
