import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'imageList.dart';
import 'imgurViewCode.dart';
import 'LogoutPage.dart';
import 'FavoritesPage.dart';
import 'SettingsPage.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  MyAppState createState() => MyAppState();
}

class MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Imgur',
      home: imgurHome(),
    );
  }
}

class imgurHome extends StatefulWidget {
  @override
  imgurHomeState createState() => imgurHomeState();
}

class imgurHomeState extends State<imgurHome> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: NavDrawer(),
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Image.asset('images/imgurLogo.png', fit: BoxFit.fill,width: 75,),
            PopupMenuButton(
                itemBuilder: (context) => [
                      PopupMenuItem(
                        value: 1,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Icon(
                              Icons.verified_user,
                              color: Colors.black,
                            ),
                            FlatButton(
                              child: Text('Profile'),
                              onPressed: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(builder: (context) => (settingsPage()),
                                  ),
                                );
                                setState(() {
                                });
                              },
                            ),
                          ],
                        ),
                      ),
                      PopupMenuItem(
                        value: 2,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Icon(
                              Icons.settings,
                              color: Colors.black,
                            ),
                            FlatButton(
                              child: Text('Settings'),
                              onPressed: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(builder: (context) => (settingsPage()),
                                  ),
                                );
                                setState(() {
                                });
                              },
                            ),
                          ],
                        ),
                      ),
                      PopupMenuItem(
                        value: 3,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Icon(
                              Icons.exit_to_app,
                              color: Colors.black,
                            ),
                            FlatButton(
                              child: Text('Logout'),
                              onPressed: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(builder: (context) => (logoutPage()),
                                  ),
                                );
                                setState(() {
                                });
                              },
                            ),
                          ],
                        ),
                      ),
                    ])
          ],
        ),
        backgroundColor: Colors.blueGrey,
      ),
      body: Center(
        child: SingleChildScrollView(
          child: Container(
            color: Color.fromRGBO(49, 53, 56, 1),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SizedBox(height: 10),
                Text('Top Images - Today', style: TextStyle(fontSize: 25, color: Color.fromRGBO(249, 255, 240, 1), fontWeight: FontWeight.bold)),
                imgurTop(row1items, row1titles, row1values),
                Text('Trending Images', style: TextStyle(fontSize: 25, color: Color.fromRGBO(249, 255, 240, 1), fontWeight: FontWeight.bold)),
                imgurTop(row2items, row2titles, row2values),
                Text('Discover', style: TextStyle(fontSize: 25, color: Color.fromRGBO(249, 255, 240, 1), fontWeight: FontWeight.bold)),
                imgurTop(row3items, row3titles, row3values),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

//Used code from https://medium.com/@maffan/how-to-create-a-side-menu-in-flutter-a2df7833fdfb
//to build the side menu of our app
class NavDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        color: Colors.green,
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              decoration: BoxDecoration(
                  color: Colors.green,
                  image: DecorationImage(
                    fit: BoxFit.fill,
                    image: AssetImage('images/imgur.png'),
                  )),
            ),
            ListTile(
              leading: Icon(Icons.verified_user),
              title: Text('Profile'),
              onTap: () => {Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => (imgurHome()),
                ),
              )},
            ),
            ListTile(
              leading: Icon(Icons.settings),
              title: Text('Settings'),
              onTap: () => {Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => (settingsPage()),
                ),
              )},
            ),
            ListTile(
              leading: Icon(Icons.favorite),
              title: Text('Favorite Images'),
              onTap: () => {Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => (favoritesPage()),
                ),
              )},
            ),
            ListTile(
              leading: Icon(Icons.exit_to_app),
              title: Text('Logout'),
              onTap: () => {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => (logoutPage()),
                  ),
                )
              }
            ),
          ],
        ),
      ),
    );
  }
}
